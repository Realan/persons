var mongoose = require('mongoose');

var personSchema = mongoose.Schema({
    firstname: String,
    surname: String,
    lastname: String,
    age: Number
});

var Person = mongoose.model('Person', personSchema);

module.exports = Person;